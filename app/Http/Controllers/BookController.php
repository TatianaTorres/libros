<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Basecontroller as BaseController;
use App\Models\Book;
use Validator;

class BookController extends BaseController
{
    public function new(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'category_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validator errors', $validator->errors());
        }

        $input = $request->all();
        $book = Book::create($input);
        $success['book'] = $book;

        return $this-> sendResponse($success, 'Book created successfully');
    }

    public function show()
    {
        $result = Book::join("categories", "categories.id","=", "books.category_id")
        ->select("books.id", "books.title", "books.author", "categories.name", "categories.details", "books.created_at", "books.updated_at");
        return $result->get();
    }
}
