<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Models\Loan;
use Illuminate\Support\Facades\DB;
use Validator;

class LoanController extends BaseController
{
    public function new(Request $request)
    {
        $validator= Validator::make($request->all(), [
            'user_id'=>'required',
            'book_id'=>'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validator error', $validator->errors());
        }

        $input=$request->all();
        $loan=Loan::create($input);
        $success['loan']= $loan;
        
        return $this->sendResponse($success, 'Loan created successfully');
    }

    public function show()
    {  
        $result = Loan::join("users", "users.id", "=", "loans.user_id")
        ->join("books", "books.id", "=", "loans.book_id")
        ->select("loans.id", "users.name", "users.email", "books.title",  "loans.created_at", "loans.updated_at");
        return $result->orderBy("loans.id")->get();
        //return Loan::paginate(10);
    }
}