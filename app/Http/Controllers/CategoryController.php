<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Controllers\BaseController as BaseController;
use App\Models\Category;
use Validator;

class CategoryController extends BaseController
{
    public function new(Request $request)
    {
        $validator=Validator::make($request->all(), [
            'name'=>'required',
            'details'=>'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validator error.', $validator->errors());    
        }

        $input = $request->all();
        $category = Category::create($input);
        $success['category']= $category;

        return $this->sendResponse($success, 'Categopry created successfully');
    }

    public function show()
    {
        return $this-> sendResponse(Category::all(), 'Catagories created successfully'); ;
    }
}