<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;

class Usercontroller extends BaseController
{
    public function new(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validator error', $validator->errors());
        }

        $input=$request->all();
        $input['password']=bcrypt($input['password']);
        $user=User::create($input);
        $success['token'] = $user->createToken('MyApp')->plainTextToken;
        $success['user']= $user;

        return $this->sendResponse($success, 'User register successfully');
    }

    public function show()
    {
        return User::all();   
    }
}