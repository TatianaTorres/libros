<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('newUser', [UserController::class, 'new']);
Route::get('users', [UserController::class, 'show']);

Route::post('newBook', [BookController::class, 'new']);
Route::get('books', [BookController::class, 'show']);

Route::post('newCategory', [CategoryController::class, 'new']);
Route::get('categories', [CategoryController::class, 'show']);

Route::post('newLoan', [LoanController::class, 'new']);
Route::get('loans', [LoanController::class, 'show']);   
