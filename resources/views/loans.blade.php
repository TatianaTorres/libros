<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Loans</title>
</head>
<body class="col-sm-10" background="https://i.pinimg.com/474x/a8/f2/15/a8f2156f06185f02291c35eb9ddbbb95.jpg">
    <h1>Prestamos</h1>
    <h3>Historial de prestamos</h3>
    <div id="app">
        <table class="table table-bordered">
            <thead>
                <th>Id</th>
                <th>Usuario</th>
                <th>Email</th>
                <th>Libro</th>
            </thead>
            <tbody>
                <tr v-for="(loan, index) in loans">
                    <td>@{{loan.id}}</td>
                    <td>@{{loan.name}}</td>
                    <td>@{{loan.email}}</td>
                    <td>@{{loan.title}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <h3>Agregar nuevo prestamo</h3>
        <form  @submit.prevent="newLoan">
            <div class="form-group col-md-4">
                <label>Usuario:</label>
                <select name="user" id="user_id" required v-model="user_id" class="form-control">
                    <option v-for="(user, index) in users"  :value="user.id" >@{{user.name}}</option>
                </select>
            </div>
            <div class="form-group col-md-4">
               <label>Libro: </label>
                <select name="book" id="book_id" required v-model="book_id" class="form-control">
                    <option v-for="(book, index) in books"  :value="book.id" >@{{book.title}}</option>
                </select>
            </div>
            <button type="submit" class="btn btn-danger">Crear</button>
        </form>
    </div>
</body>
<script>

var app = new Vue({
  el: '#app',
  data: {
    loans: [],
    books: [],
    users: [],
    user_id: null,
    book_id: null
  },
  mounted() {
      this.getBooks()
      this.getUsers()
      this.getLoans()
  },
  
  methods: {
      getLoans(){
          let data= fetch("/api/loans").then(async (res)=>{
          let result = await res.json() 
          this.loans = result
          })
    },

      getUsers(){
        let data= fetch("/api/users?page=1").then(async (res)=>{
            let result = await res.json()
            this.users = result.data
        })
    },

      getBooks(){
        let data= fetch("/api/books?page=1").then(async (res)=>{
            let result = await res.json()
            this.books = result.data
        })
    },

      newLoan(){
        let user_id = this.user_id
        let book_id = this.book_id

        let headers = {"Content-Type":'application/json'}
        let data= fetch("/api/newLoan", {
            headers, 
            method: 'POST', 
            body: JSON.stringify({user_id,book_id}) 
        }).then(async (res)=>{
            let result = await res.json()
            this.getLoans()
        })
        console.log(user_id,book_id);
    }

  }
})

</script>
</html>