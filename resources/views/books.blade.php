<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Books</title>
</head>
<body class="col-sm-10" background="https://i.pinimg.com/474x/a8/f2/15/a8f2156f06185f02291c35eb9ddbbb95.jpg">
    <h1>Libros</h1>
    <h3>Lista de libros disponibles</h3>
    <div id="app">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Titulo</th>
                    <th scope="col">Autor</th>
                    <th scope="col">Categoria</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(book, index) in books">
                    <td >@{{ book.id }}</td>
                    <td>@{{ book.title }}</td>
                    <td>@{{ book.author }}</td>
                    <td>@{{ book.name }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <h3>Agregar nuevo libro</h3>
        <form @submit.prevent="createBooks">
            <div class="form-group col-md-4">
                <input type="text" name="title" id="title" placeholder="Titulo" required v-model="title" class="form-control">
            </div>
            <div class="form-group col-md-4">
                <input type="text" name="author" id="author" placeholder="Autor" required v-model="author" class="form-control"> 
            </div>
            <div class="form-group col-md-4">
                <select name="category" id="category" required v-model="category_id" class="form-control">
                    <option v-for="(category, index) in categories"  :value="category.id" >@{{category.name}}</option>
                </select>
                <a href="/categories">Agregar genero</a>
            </div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-danger">Crear</button>
                <br> 
            </div>
        </form>    
    </div>
</body>
<script>

var app = new Vue({
  el: '#app',
  data: {
    categories: [],
    books: [],
    title: '',
    author: '',
    category_id: null
  },
  mounted() {
      this.getBooks()
      this.getCategories()
  },

  methods: {
    getBooks(){
        let data= fetch("/api/books").then(async (res)=>{
            let result = await res.json()
            this.books = result
        })
    },

    getCategories(){
        let data= fetch("/api/categories").then(async (res)=>{
            let result = await res.json()
            this.categories = result
        })
    },

    createBooks(){
        let title = this.title
        let author = this.author
        let category_id = this.category_id


        let headers = {"Content-Type":'application/json'}
        let data= fetch("/api/newBook", {
            headers,
            method: 'POST',
            body: JSON.stringify({title,author,category_id}),
        }).then(async (res)=>{
            let result = await res.json()
            this.getBooks()  
        })
        console.log(title,author,category_id, headers);
    }

  }

})

</script>
</html>