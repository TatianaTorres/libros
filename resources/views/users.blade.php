<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Users</title>
</head>
<body class="col-sm-10" background="https://i.pinimg.com/474x/a8/f2/15/a8f2156f06185f02291c35eb9ddbbb95.jpg">
    <h1>Usuarios</h1>
    <h3>Lista de usuarios registrados</h3>
    <div id="app">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th >
                    <th>Nombre</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(user, index) in users">
                    <td >@{{user.id}}</td>
                    <td>@{{user.name}}</td>
                    <td>@{{user.email}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <h3>Crear un usuario nuevo</h3>
        <form @submit.prevent="createUser">
             <div class="form-group col-md-4">
                <label>Nombre:</label>
                <input class="form-control" type="text" name="name" id="name" placeholder="Nombre" required v-model="name">
            </div>
            <div class="form-group col-md-4">
                <label>Email:</label>
                <input class="form-control" type="text" name="email" id="email" placeholder="Email" required v-model="email">
            </div>
            <div class="form-group col-md-4">
                <label>Contraseña:</label>
                <input class="form-control" type="password" name="password" id="password" placeholder="Contraseña" required v-model="password">
            </div>
            <div class="form-group col-md-4">
                <label>Confirmar contraseña:</label>
                <input class="form-control" type="password" name="c_password" id="c_password" placeholder="Confirmar contraseña" required v-model="c_password">
            </div>
            <button class="btn btn-danger" type="submit">Crear</button>
        </form>
    </div>
</body>
<script>

var app = new Vue({
  el: '#app',
  data: {
    users: [],
    name: '',
    email: '',
    password: '',
    c_password: ''
  },
  mounted() {
      this.getUsers()
  },
  
  methods: {
    getUsers(){
        let data= fetch("/api/users").then(async (res)=>{
            let result = await res.json()
            this.users = result
        })
    },

    createUser(){
        let name = this.name
        let email = this.email
        let password = this.password
        let c_password = this.c_password

        let headers = {"Content-Type":'application/json'}
        let data= fetch("/api/newUser", {
            headers,
            method: 'POST',
            body: JSON.stringify({name,email,password, c_password}),
        }).then(async (res)=>{
            let result = await res.json()
            this.getUsers()  
        })
        console.log(name,email,password, c_password);
    }

  }

})
</script>
</html>