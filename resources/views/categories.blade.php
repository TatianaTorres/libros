<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Categories</title>
</head>
<body class="col-sm-10" background="https://i.pinimg.com/474x/a8/f2/15/a8f2156f06185f02291c35eb9ddbbb95.jpg">
    <h1>Generos</h1>
    <h3>Lista de generos registrados</h3>
    <div id="app">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Detalles</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(category, index) in categories">
                    <td>@{{category.id}}</td>
                    <td>@{{category.name}}</td>
                    <td>@{{category.details}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <h3>Agregar nuevo genero</h3>
        <form @submit.prevent="createNew">
            <div class="form-group col-md-4">
                <input type="text" name="name" id="name" placeholder="Nombre" required v-model="name" class="form-control">
            </div>
            <div class="form-group col-md-4">
                <textarea type="text" name="details" id="details" placeholder="Detalles" required v-model="details" class="form-control"></textarea>
            </div>
            <button type="submit"class="btn btn-danger">Agregar</button>
        </form>    
    </div>
</body>
<script>

var app = new Vue({
  el: '#app',
  data: {
    categories: [],
    name: '',
    details: ''
  },
  mounted() {
      this.getCategory()
  },
  
  methods: {
    getCategory(){
        let data= fetch("/api/categories").then(async (res)=>{
            let result = await res.json()
            this.categories = result.data
        })
    },

    createNew(){
        let name = this.name
        let details = this.details
       
        let headers = {"Content-Type":'application/json'}
        let data= fetch("/api/newCategory", {
            headers,
            method: 'POST',
            body: JSON.stringify({name,details}),
        }).then(async (res)=>{
            let result = await res.json()
            this.getCategory()  
        })
        console.log(name,details);
    }

  }

})
</script>
</html>